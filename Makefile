build:

install:
	install -d -m 755 $(DESTDIR)/usr/bin
	install -d -m 755 $(DESTDIR)/usr/sbin
	install -m755 upgrade-porter-chroots $(DESTDIR)/usr/sbin
	install -m755 apache2-vhost-update $(DESTDIR)/usr/sbin
	install -m755 buildd-reboot $(DESTDIR)/usr/sbin
	install -m755 portforwarder-ssh-wrap $(DESTDIR)/usr/bin
	install -m755 apt-in-chroot $(DESTDIR)/usr/bin
